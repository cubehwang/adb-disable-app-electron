import {
  PACKAGE_CLEAR_REQUEST,
  PACKAGE_LIST_REQUEST,
  PACKAGE_LIST_SUCCESS,
  PACKAGE_LIST_ERROR,
  PACKAGE_ENABLE_REQUEST,
  PACKAGE_ENABLE_PROGRESS,
  PACKAGE_ENABLE_DONE,
  PACKAGE_DISABLE_REQUEST,
  PACKAGE_DISABLE_PROGRESS,
  PACKAGE_DISABLE_DONE,
} from '@/store/actions';

import {listPackages, disablePackage, enablePackage} from '@/utils/adb.js';

const state = {
  status: null,
  error: null,
  packages: [],
  info: [],
};

const getters = {
  packages: (state) => state.packages,
  actionInfo: (state) => state.info,
};

const actions = {
  [PACKAGE_CLEAR_REQUEST]({commit}) {
    commit(PACKAGE_CLEAR_REQUEST);
  },
  [PACKAGE_LIST_REQUEST]({commit}) {
    commit(PACKAGE_LIST_REQUEST);
    listPackages(
      (items) => commit(PACKAGE_LIST_SUCCESS, items),
      (err) => commit(PACKAGE_LIST_ERROR, err),
    );
  },
  async [PACKAGE_DISABLE_REQUEST]({commit}, items) {
    commit(PACKAGE_DISABLE_REQUEST);
    for (let item of items) {
      const r = await new Promise((resolve) => {
        disablePackage(item.name, (info) => resolve({name: item.name, info}));
      });
      commit(PACKAGE_DISABLE_PROGRESS, r);
    }
    commit(PACKAGE_DISABLE_DONE);
  },
  async [PACKAGE_ENABLE_REQUEST]({commit}, items) {
    commit(PACKAGE_ENABLE_REQUEST);
    for (let item of items) {
      const r = await new Promise((resolve) => {
        enablePackage(item.name, (info) => resolve({name: item.name, info}));
      });
      commit(PACKAGE_ENABLE_PROGRESS, r);
    }
    commit(PACKAGE_ENABLE_DONE);
  },
};

const mutations = {
  [PACKAGE_CLEAR_REQUEST](state) {
    state.status = null;
    state.error = null;
    state.info = [];
  },
  [PACKAGE_LIST_REQUEST](state) {
    state.status = PACKAGE_LIST_REQUEST;
    state.error = null;
    state.packages = [];
  },
  [PACKAGE_LIST_SUCCESS](state, items) {
    state.status = PACKAGE_LIST_SUCCESS;
    state.packages = items;
  },
  [PACKAGE_LIST_ERROR](state, err) {
    state.status = PACKAGE_LIST_ERROR;
    state.error = err;
  },
  [PACKAGE_DISABLE_REQUEST](state) {
    state.status = PACKAGE_DISABLE_REQUEST;
    state.error = null;
    state.info = [];
  },
  [PACKAGE_ENABLE_REQUEST](state) {
    state.status = PACKAGE_ENABLE_REQUEST;
    state.error = null;
    state.info = [];
  },
  [PACKAGE_DISABLE_PROGRESS](state, info) {
    state.status = PACKAGE_DISABLE_PROGRESS;
    state.info.push(info);
  },
  [PACKAGE_ENABLE_PROGRESS](state, info) {
    state.status = PACKAGE_ENABLE_PROGRESS;
    state.info.push(info);
  },
  [PACKAGE_DISABLE_DONE](state) {
    state.status = PACKAGE_DISABLE_DONE;
  },
  [PACKAGE_ENABLE_DONE](state) {
    state.status = PACKAGE_ENABLE_DONE;
  },
};

export default {state, getters, actions, mutations};
