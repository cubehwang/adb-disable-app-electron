import {exec} from 'child_process';

const basePath = process.env.VUE_APP_ADB_PATH || '';

export function listPackages(cbSuccess, cbError) {
  const cmd = `${basePath}adb shell pm list packages`;
  exec(cmd, (_, stdout, stderr) => {
    if (stdout) {
      cbSuccess(
        stdout
          .split('\n')
          .filter((s) => s)
          .map((line) => line.replace('package:', ''))
          .sort((a, b) => a.localeCompare(b)),
      );
    }
    if (stderr) {
      cbError(stderr);
    }
  });
  return [];
}

export function disablePackage(name, cb) {
  const cmd = `${basePath}adb shell pm disable-user --user 0 ${name}`;
  exec(cmd, (_, stdout, stderr) => {
    if (stdout) cb(stdout);
    if (stderr) cb(stderr);
  });
}

export function enablePackage(name, cb) {
  const cmd = `${basePath}adb shell pm enable ${name}`;
  exec(cmd, (_, stdout, stderr) => {
    if (stdout) cb(stdout);
    if (stderr) cb(stderr);
  });
}
